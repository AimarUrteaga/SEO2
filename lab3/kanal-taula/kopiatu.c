/* sarrera estandarretik jasotzen dena irteera estandarrean idazten da,
 * BUFSIZE aldagaiak adierazten duen tamainarekin multzokatuz,
 * eta read eta write deiak erabiliz. */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFSIZE 512
#define STDIN_FILENO  0 // <unistd.h> fitxategian ere
#define STDOUT_FILENO 1 // definituta daude

int main (int argc, char *argv[]) {
   int n;
   char buffer[BUFSIZE];

   /* irakurketa eta idazketa zikloa */
   while ((n = read(STDIN_FILENO, buffer, BUFSIZE)) > 0)
	sleep(300);
      if (write(2, buffer, n) != n) perror("write");
   return 0;
}
