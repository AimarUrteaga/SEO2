#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h> 
#include <sys/time.h> 
#include <sys/stat.h>
  
#define errore(a) {perror(a); exit(1);};
#define BUFSIZE 1024

int ausaz(int eremua) { 
    struct timeval t; 
    static int i=0; 
    // i=0 adierazten du funtzioa lehenengo aldiz exekutatzen dela
 
    if (i==0) { // hazaia (seed) ezarri
        gettimeofday(&t, NULL); 
        srand(t.tv_sec); // auazako 
    } 
    i = 1; // hurrengo exekuzioetan ez da beharrezkoa hazia ezartzea
    return(rand() % eremua); 
} 

int main (int argc, char *argv[]) {
    char *fitx_izena;       // Fitxategiaren izena helbideratzeko
    FILE *fitx_stream;      // Fitxategia kudeatzeko erakuslea "stream" motako egiturara
    char lerroa[BUFSIZE];   // Fitxategian irakurritako lerroak helbideratzeko
    int  ausazko_zbk;
    int  lerro_kop;
    int  kont;

    //fitx_izena = argv[1];   // parametro moduan jasotako fitxategiaren izena
    fitx_izena = "esaerak.txt";
    // Egiaztatu fitxategia existitzen dela eta fitxategi arrunta dela
    struct stat stat_egitura;
    if (stat(fitx_izena, &stat_egitura) != 0 || ! S_ISREG(stat_egitura.st_mode)) {
        printf("Lehenengo parametroa: %s ez da fitxategi arrunta\n", fitx_izena);
        errore("Parametroa");
    }

    if ((fitx_stream= fopen(fitx_izena, "r")) == NULL) errore("open"); // Ireki fitxategia

    /* Lerro kopurua kalkulatu lerro guztiak irakurriz */
    lerro_kop=0;
    while (fgets(lerroa, BUFSIZE, fitx_stream) != NULL ) { // Irakurri lerroka
      lerro_kop++;
    }
    if (lerro_kop==0) {
      printf("Fitxategia hutsik dago\n");
      exit(1);
    }
    ausazko_zbk = ausaz(lerro_kop); // Lortu ausazko zenbakia

    fseek(fitx_stream,0,SEEK_SET);  // Hasierarako posizioara itzuli

    kont=0;
    while (fgets(lerroa, BUFSIZE, fitx_stream) != NULL ) { // Irakurri lerroka
      if (kont==ausazko_zbk) {
        printf("%s",lerroa);
        break;
      }
      kont++;
    }

    fclose(fitx_stream);            // Itxi fitxategia
    return(0);
}
