import sys
import random

fitx_izena  = sys.argv[1]       # Parametro moduan jasotako fitxategiaren izena

fitx_obj    = open(fitx_izena, "r") # Ireki fitxategia

# Lerro kopurua kalkulatu lerro guztiak irakurriz 
lerro_kop=0;
for lerroa in fitx_obj:         # Irakurri lerroka banan-banan zenbatzeko
  lerro_kop = lerro_kop + 1
if lerro_kop==0:
  print("Fitxategia hutsik dago")
  sys.exit(1)

ausazko_zbk = random.randrange(lerro_kop)   # Lortu ausazko zenbakia

fitx_obj.seek(0)                # Hasierako posizioara itzuli

kont=0;
for lerroa in fitx_obj:         # Irakurri lerroka, ausazko posizioan
  if (kont == ausazko_zbk) :  #  dagoen esaera aurkitzeko
    print(lerroa)
    break
  kont = kont + 1

fitx_obj.close()                # Itxi fitxategia
