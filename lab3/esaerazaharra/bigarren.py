import sys
import random

fitx_izena=sys.argv[1]          # Parametro moduan jasotako fitxategiaren izena

fitx_obj = open(fitx_izena, "r") # Ireki fitxategia
lerroak  = fitx_obj.readlines()  # Irakurri lerro guztiak batera eta sartu "lerroak" zerrendan
fitx_obj.close()                # Itxi fitxategia

lerro_kop   = len(lerroak)      # Lortu lerro kopurua (zerrendaren elementu kopurua)
ausazko_zbk = random.randrange(lerro_kop) # Lortu ausazko zenbakia

print(lerroak[ausazko_zbk])     # Inprimatu ausazko posizioan dagoen esaera
