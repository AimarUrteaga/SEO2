/* jaurti1.c: komando interpretatzailea */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include<sys/wait.h>
#include <string.h>

#define errore(a) {perror(a); exit(1);};
#define BUFSIZE 512
#define MAXARG 10

int lortu_argumentuak(char *buf, int n, char *argk[], int ma);
/* Erabiltzaileak idatzitako komandoa eta argumentuak irakurtzeko funtzioa, */
/* arg[],argc formatuan egituratzeko  */

int main(int argc, char *argv[]) {
  int err, n, status, fork_emaitza, pid, pid_gurasoa, pid_umea, pid_finished,
      umea_itzulera_kodea;
  char buf[BUFSIZE];
  char *arg[MAXARG];


//char *gonbidapen="jaurti6";


  for (n = 0; n < BUFSIZE; n++) buf[n] = '\0'; //
  /* irakurketa */
  if (getenv("GONBIDAPEN")==NULL){
  	setenv("GONBIDAPEN","jaurti6",1);
  }
  
  //gonbidapen=getenv("GONBIDAPEN");
  write(1, getenv("GONBIDAPEN"), strlen(getenv("GONBIDAPEN")));       // aurkeztu prompt-a
  write(1, "$ ", 2);
  while ((n = read(0, buf, BUFSIZE)) > 0){ // irakurri erabiltzailearen agindua
    if (n>1) {                    // lerro-jauzia baino zerbait gehiago sartu da
      buf[n] = '\n'; n++;         // lerro-jauzia gehitzen da bukaeran, geroago
      err = lortu_argumentuak(buf, n, arg, MAXARG);
      if (0==strcmp(arg[0], "exit")){
      	 printf("Programa amaitzen\n");
      	return 0;
      }
      if (0==strcmp(arg[0], "esleitu")){
      	 setenv(arg[1],arg[3],1);
      }else {
	      if (*arg[0]!='&' & *arg[0]!=';'){
		  	printf("Ez dauka asiera egokia\n");
		  	return 1;
	      }
	    //  }
      fork_emaitza = fork();      // sortu prozesua
      switch (fork_emaitza){ 
        case -1: errore("fork");
          break;
        case  0: /* fork funtzioak 0 itzultzen du prozesu berriaren kasuan */
          /* umearen kodea */
          /* erabiltzailearen programa exekutatu */
          pid = getpid();
          pid_gurasoa = getppid();
          printf("-U------- Umea naiz: sortu berria (PID %d - Parent PID: %d): orain '%s ...' exekutatzera...\n", pid, pid_gurasoa, arg[1]);
          if (*arg[0]='&'){
          
          	fork_emaitza = fork();
          	switch (fork_emaitza){
			case -1: errore("fork");
			  break;
			case  0://umea
				execvp(arg[1], &arg[1]);
				//matar al padre escribir mensaje de que ha salido ok
			default:
				//esperar y chequear el estado escribir mensaje de fallo
				sleep(5);
          			int estado=waitpid(fork_emaitza, &status, WNOHANG);
          			if (estado!=0){        // Jaso itzulera kodea
					umea_itzulera_kodea = WEXITSTATUS(status);
					printf("-G------- Gurasoa naiz (PID %d): prozesu ume bat (PID %d) bukatu da %d itzulera-kodearekin\n", pid, fork_emaitza, umea_itzulera_kodea);
					return 0;
           			 }else{
           			 	kill(fork_emaitza, SIGKILL);
					printf("-G------- Gurasoa naiz (PID %d): prozesu ume bat (PID %d) ez dio bukatzeko denborarik eman bukatu.", pid, pid_finished);
           				return -1;
           			 }
          	  }	
            
          
          }else{ 
          execvp(arg[1], &arg[1]);
          }
          printf("-U------- Umea naiz (PID %d): puntu honetan soilik errorea egonez gero\n",
                pid);
          errore("exec");
          break;
        default: /* gurasoaren kodea */
          pid_umea = fork_emaitza;
          pid = getpid();
          printf("-G------- Gurasoa naiz (PID %d): prozesu umea '%s ...' (PID %d) abiatu da\n", 
                pid, arg[1], pid_umea);
          /* umea bukatu arte itxoin */
          
	if (*arg[0]!='&'){
          pid_finished = wait(&status);
          if ( pid_finished != pid_umea) { // bukatu den prozesua aurretik sortutakoa bada
              errore("wait");
          }else {
            if (WIFEXITED(status)){        // Jaso itzulera kodea
              umea_itzulera_kodea = WEXITSTATUS(status);
              printf("-G------- Gurasoa naiz (PID %d): prozesu ume bat (PID %d) bukatu da %d itzulera-kodearekin\n", pid, pid_finished, umea_itzulera_kodea);
            }
          }
	}
          break;
      }
      }
    }
    for (n = 0; n < BUFSIZE; n++) buf[n] = '\0';
    write(1, getenv("GONBIDAPEN"), strlen(getenv("GONBIDAPEN")));       // aurkeztu prompt-a
  write(1, "$ ", 2);
  
  }
  printf("\n");
  exit(0);
}

int lortu_argumentuak(char *buf, int n, char *argk[], int m) {
  int i, j;

  for (i = 0, j = 0; (i < n) && (j < m); j++) {
    /* zuriuneak pasa */
    while (((buf[i] == ' ') || (buf[i] == '\n')) && (i < n)) i++;
    if (i == n) break;
    argk[j] = &buf[i];
    /* bilatu zuriune karakterea */
    while ((buf[i] != ' ') && (buf[i] != '\n')) i++;
    buf[i++] = '\0';
  }

  argk[j] = NULL;
  return 0;
}
