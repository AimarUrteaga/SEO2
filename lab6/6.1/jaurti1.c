/* jaurti1.c: komando interpretatzailea */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#define errore(a) {perror(a); exit(1);};
#define BUFSIZE 512
#define MAXARG 10

int lortu_argumentuak(char *buf, int n, char *argk[], int ma);
/* Erabiltzaileak idatzitako komandoa eta argumentuak irakurtzeko funtzioa, */
/* arg[],argc formatuan egituratzeko  */

int main(int argc, char *argv[]) {
  int err, n, status, fork_emaitza, pid, pid_gurasoa, pid_umea, pid_finished,
      umea_itzulera_kodea;
  char buf[BUFSIZE];
  char *arg[MAXARG];

  for (n = 0; n < BUFSIZE; n++) buf[n] = '\0'; //
  /* irakurketa */
  write(1, "jaurti1$ ", 9);       // aurkeztu prompt-a
  while ((n = read(0, buf, BUFSIZE)) > 0){ // irakurri erabiltzailearen agindua
    if (n>1) {                    // lerro-jauzia baino zerbait gehiago sartu da
      buf[n] = '\n'; n++;         // lerro-jauzia gehitzen da bukaeran, geroago
      err = lortu_argumentuak(buf, n, arg, MAXARG);
      fork_emaitza = fork();      // sortu prozesua
      switch (fork_emaitza){ 
        case -1: errore("fork");
          break;
        case  0: /* fork funtzioak 0 itzultzen du prozesu berriaren kasuan */
          /* umearen kodea */
          /* erabiltzailearen programa exekutatu */
          pid = getpid();
          pid_gurasoa = getppid();
          printf("-U------- Umea naiz: sortu berria (PID %d - Parent PID: %d): orain '%s ...' exekutatzera...\n", 
                pid, pid_gurasoa, arg[0]);
          execvp(arg[0], arg);
          printf("-U------- Umea naiz (PID %d): puntu honetan soilik errorea egonez gero\n",
                pid);
          errore("exec");
          break;
        default: /* gurasoaren kodea */
          pid_umea = fork_emaitza;
          pid = getpid();
          printf("-G------- Gurasoa naiz (PID %d): prozesu umea '%s ...' (PID %d) abiatu da\n", 
                pid, arg[0], pid_umea);
          /* umea bukatu arte itxoin */
          pid_finished = wait(&status);
          if ( pid_finished != pid_umea) { // bukatu den prozesua aurretik sortutakoa bada
              errore("wait");
          } else {
            if (WIFEXITED(status)){        // Jaso itzulera kodea
              umea_itzulera_kodea = WEXITSTATUS(status);
              printf("-G------- Gurasoa naiz (PID %d): prozesu ume bat (PID %d) bukatu da %d itzulera-kodearekin\n", 
                pid, pid_finished, umea_itzulera_kodea);
            }
          };
          break;
      }
    }
    for (n = 0; n < BUFSIZE; n++) buf[n] = '\0';
    write(1, "jaurti1$ ", 9);
  }
  printf("\n");
  exit(0);
}

int lortu_argumentuak(char *buf, int n, char *argk[], int m) {
  int i, j;

  for (i = 0, j = 0; (i < n) && (j < m); j++) {
    /* zuriuneak pasa */
    while (((buf[i] == ' ') || (buf[i] == '\n')) && (i < n)) i++;
    if (i == n) break;
    argk[j] = &buf[i];
    /* bilatu zuriune karakterea */
    while ((buf[i] != ' ') && (buf[i] != '\n')) i++;
    buf[i++] = '\0';
  }

  argk[j] = NULL;
  return 0;
}
