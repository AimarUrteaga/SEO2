# Script trazen sortzea automatitzatzeko (hiru programazio lengoaietan)
# Parametro bat jaso dezake; zenbat zenbaki idatzi behar duten 
# Parametrorik jaso ezezean, behin baino ez da idazten.

KOP=1               # Segidaren muga: defektuzko balioa
if [ $# -eq 1 ]
then
  KOP=$1          # Parametroa jasoz gero (suposatuz zenbakia dela)
fi
# sistema-deien inguruko informazioa exekutatu beharreko komandoa
STRACE="/usr/bin/strace -f -e trace=write -qq -q" 
# emaitza gordeko den fitxategien izenen patroia
KOPTEXT=$(printf "%08d\n" $KOP)  

# Programazio lengoaia bakoitzeko:
# - Lortu exkuzioaren sistema-deiak (strace programa kop)
# - Idazten den balio segida (1 2 3 ... ) ez erakutsi pantaialan (>/dev/null)
# - Eskuratu soilik irteera estandarrean idazten duten sistema-deiak (grep 'write(1')
# - Gorde emaitzak 'trace-kop-lengoia' patroia duen fitxategi batean
# programen irteera ez da interesgarria, beraz, /dev/null dispositibora
$STRACE ./segida          $KOP 2>&1 >/dev/null | grep "write(1" >trace-${KOPTEXT}-c    # Java-rako
$STRACE java segida       $KOP 2>&1 >/dev/null | grep "write(1" >trace-${KOPTEXT}-java # C-rako
$STRACE python3 segida.py $KOP 2>&1 >/dev/null | grep "write(1" >trace-${KOPTEXT}-py   # Python-erako

