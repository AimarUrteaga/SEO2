#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <stdlib.h>


#define BUFSIZE 512
#define entradapipename "./entradaelpipealserver"
#define mascara 0777


struct bloque
{
   char localizacion [BUFSIZE];
   int pid;
};


int main(int argc, char *argv[]) {
	 signal(SIGCHLD,SIG_IGN);
	if (mkfifo(entradapipename, mascara)==-1) {
		printf("Eentradaelpipealserver jada existitzen sen\n");
	}
	int filedescriptorentrada; // = open(entradapipename,O_RDONLY);//La cojo en modo lectura
	struct bloque entrada;
	char buelta [BUFSIZE];
	char pid [7];
	while (1){
		/*if (fork()==0){//genero un doblo proceso uno lo mando al principio y el otro lo debulebo
			continue;
		}*/
		filedescriptorentrada = open(entradapipename,O_RDONLY);//La cojo en modo lectura
		if (read(filedescriptorentrada,&entrada,sizeof(struct bloque))==-1){//leo el pipe y miro si hay algo malo
			printf("Arazo larri bat agertu da\n");
			continue;
		}
		close(filedescriptorentrada);//lo cierro despues escribir
		sprintf(pid, "%d",entrada.pid);//combierto el pid en string
		printf("Egin beharra jasota procesatzen...\n");
		entrada.localizacion[strlen(entrada.localizacion)-1] = '\0';//poner final de linea
		sleep (10);
		printf("%s artziboa irekitzen ...\n",entrada.localizacion);
		FILE *fp = fopen(entrada.localizacion, "r");//harbro fitzategi
		int filedescriptorslida = open(pid,O_WRONLY);
		if (fp==NULL){//si no existe el archibo
			write(filedescriptorslida,"",0);//escribo bacio
			close(filedescriptorslida);//lo cierro despues escribir
			printf("Artziboa ez da existitzen\n");
			continue;
		}
		fgets(buelta, sizeof(char)*BUFSIZE, fp);
		fclose(fp);//lo cierro despues leher
		printf("%s",buelta);
		write(filedescriptorslida,buelta,sizeof(char)*BUFSIZE);
		close(filedescriptorslida);//lo cierro despues escribir
		//return 0;
	}
	
	
}
