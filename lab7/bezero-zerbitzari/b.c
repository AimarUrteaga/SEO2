
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>


#define BUFSIZE 512
#define entradapipename "./entradaelpipealserver"
#define mascara 0777

struct bloque
{
   char localizacion [BUFSIZE];
   int pid;
};


int main(int argc, char *argv[]) {
	char pid [7];
	char buelta [BUFSIZE];
	int filedescriptorslida = open(entradapipename,O_WRONLY);//La cojo en modo escritura
	//if (filedescriptorslida==0){
	//char entrada [BUFSIZE];
	printf("Artziboaren elbidea: ");
	struct bloque salida;
	fgets(salida.localizacion, BUFSIZE, stdin);
	salida.pid=getpid();
	sprintf(pid, "%d",salida.pid);//combierto el pid en string
	mkfifo(pid, mascara);
	write(filedescriptorslida,&salida,sizeof(struct bloque));
	close(filedescriptorslida);
	int filedescriptorentrada = open(pid,O_RDONLY);//La cojo en modo escritura
	if (read(filedescriptorentrada,buelta,sizeof(char)*BUFSIZE)==-1){//leo el pipe y miro si hay algo malo
		printf("Arazo larri bat agertu da\n");
		return 1;
	}
	close(filedescriptorentrada);
	printf("Serbitzariak urrengoa itzuli du:\n %s",buelta);
	remove(pid);
	return 0;
}
